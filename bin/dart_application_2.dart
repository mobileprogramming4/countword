import 'package:dart_application_2/dart_application_2.dart'
    as dart_application_2;

void main() {
  String? paragraph =
      "There are two basic purposes behind education. The first is to free people from ignorance, superstition, bad habits, and many wrong ideas. Secondly, to provide the citizens of a country with some skill or special kind of knowledge that would enable them to earn a decent living. In a highly populated country like India education is a must for both the purposes mentioned. First, there must be a hundred per cent literacy if the so-called democracy that the constitution guarantees for its citizens is to have any true meaning. Only educated citizens can utilize democratic rights usefully.";

  paragraph = paragraph.toLowerCase();
  paragraph = paragraph.replaceAll(",", "");
  paragraph = paragraph.replaceAll(".", "");
  List<String> list = paragraph.split(" ");
  List<String> word = [];
  List<int> count = [];

  for (int i = 0; i < list.length; i++) {
    var check = false;
    for (int j = 0; j < word.length; j++) {
      if (list[i] == word[j]) {
        check = true;
        count[j]++;
        break;
      }
    }
    if (check == false) {
      word.add(list[i]);
      count.add(1);
    }
  }
  int num = 1;
  for (int i = 0; i < word.length; i++) {
    print("$num. ${word[i]} ${count[i]}");
    num++;
  }
}
